package tech.dhimas.operatingsystems;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class OSAdapter extends RecyclerView.Adapter<OSAdapter.OSViewHolder> {

    public class OSViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView mOSLogo;
        TextView mOSName;

        public OSViewHolder(@NonNull View itemView) {
            super(itemView);

            mOSLogo = itemView.findViewById(R.id.iv_os_logo);
            mOSName = itemView.findViewById(R.id.tv_os_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    private LayoutInflater mInflater;
    private ArrayList<OS> oses;

    public OSAdapter() {
        this.oses = new ArrayList<>();
    }

    public OSAdapter(ArrayList<OS> oses) {
        this.oses = oses;
    }

    @NonNull
    @Override
    public OSViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        this.mInflater = LayoutInflater.from(parent.getContext());

        View itemView = this.mInflater.inflate(R.layout.os_item, parent, false);

        return new OSViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OSViewHolder osViewHolder, final int i) {
        final OS current = this.oses.get(i);
        osViewHolder.mOSName.setText(current.getName());
        osViewHolder.mOSLogo.setImageResource(current.getPhotoResId());
        osViewHolder.mOSLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mInflater.getContext(), current.getName(), Toast.LENGTH_LONG).show();
                disposeOS(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.oses.size();
    }

    private void disposeOS(int pos) {
        this.oses.remove(pos);
        this.notifyDataSetChanged();
    }
}
