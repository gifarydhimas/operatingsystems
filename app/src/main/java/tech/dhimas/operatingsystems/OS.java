package tech.dhimas.operatingsystems;

public class OS {
    private String name;
    private int photoResId;

    public OS(String name, int photoResId) {
        this.name = name;
        this.photoResId = photoResId;
    }

    public String getName() {
        return name;
    }

    public int getPhotoResId() {
        return photoResId;
    }
}
