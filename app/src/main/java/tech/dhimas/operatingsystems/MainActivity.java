package tech.dhimas.operatingsystems;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{

    private Button mButtonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<OS> oses = new ArrayList<>();
        resetOses(oses);

        RecyclerView mRecyclerView = findViewById(R.id.rcv_oses);
        final OSAdapter mAdapter = new OSAdapter(oses);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mButtonReset = findViewById(R.id.bt_reset);
        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetOses(oses);
                mAdapter.notifyDataSetChanged();
            }
        });

    }

    private void resetOses(@NonNull ArrayList<OS> oses) {
        oses.clear();
        oses.add(new OS("iOS", R.drawable.apple));
        oses.add(new OS("Android", R.drawable.android));
        oses.add(new OS("macOS", R.drawable.apple));
        oses.add(new OS("Windows", R.drawable.windows));
        oses.add(new OS("Debian", R.drawable.debian));
        oses.add(new OS("Arch Linux", R.drawable.archlinux));
        oses.add(new OS("OpenSUSE", R.drawable.opensuse));
        oses.add(new OS("Ubuntu", R.drawable.ubuntu));
        oses.add(new OS("FreeBSD", R.drawable.freebsd));
        oses.add(new OS("Fedora", R.drawable.fedora));
        oses.add(new OS("Alpine Linux", R.drawable.alpinelinux));
        oses.add(new OS("Linux Mint", R.drawable.linuxmint));
    }

}
